FROM node:latest

WORKDIR /opt

RUN git clone https://github.com/ansible-community/ara-web

WORKDIR ara-web

RUN npm install

EXPOSE 3000
CMD [ "npm", "start" ]