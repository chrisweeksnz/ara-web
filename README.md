# ara-web

ara-web requires a running instance of [ara](https://gitlab.com/chrisweeksnz/ara).

# run

```shell
docker run \
  -d \
  --net=host \
  --name=ara-web \
  registry.gitlab.com/chrisweeksnz/ara-web:latest
```